import React, { Component } from 'react';
import { View, Text, ScrollView, FlatList,StyleSheet, Picker, Switch, Modal, Alert, PanResponder, Share } from 'react-native';
import { Card, Icon, Input, Rating,Button } from 'react-native-elements';
import { connect } from 'react-redux';
import { baseUrl } from '../shared/baseUrl';
import { postFavorite,postComment } from '../redux/ActionCreators';
import * as Animatable from 'react-native-animatable';


const mapStateToProps=state => {
    return {
        dishes : state.dishes,
        comments : state.comments,
        favorites: state.favorites
        
        
    }
}

const mapDispatchToProps = dispatch => ({
    postFavorite: (dishId) => dispatch(postFavorite(dishId)),
    postComment: (dishId, rating, author, comment) => dispatch(postComment(dishId, rating, author, comment))
})

function RenderDish(props) {

    const dish = props.dish;
    handleViewRef = ref => this.view = ref;
    
    const recognizeDrag = ({ moveX, moveY, dx, dy }) => {
        
        if ( dx > 0 && dx <= 200 ){
        
            return true;
        }
        else{
        
            return false;
        }
    }

    const recognizeComment = ({ moveX, moveY, dx, dy }) => {
        
        if ( dx < 0 ){
        
            return true;
        }
        else{
        
            return false;
        }
    }

    const panResponder = PanResponder.create({
        onStartShouldSetPanResponder: (e, gestureState) => {
            
            return true;
        },
        onPanResponderGrant: () => {
        this.view.rubberBand(1000)
        .then(endState => console.log(endState.finished ? 'finished' : 'cancelled'));
    },
        onPanResponderEnd: (e, gestureState) => {
            console.log("pan responder end", gestureState);
            if (recognizeDrag(gestureState))
            
                Alert.alert(
                    'Add Favorite',
                    'Are you sure you wish to add ' + dish.name + ' to favorite?',
                    [
                    {text: 'Cancel', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                    {text: 'OK', onPress: () => {props.favorite ? console.log('Already favorite') : props.onPress()}},
                    ],
                    { cancelable: false }
                );
            
                if(recognizeComment(gestureState))
                {
                    console.log('Inside RecogComment');
                    props.onPressComments();

                }

            return true;
        }
    });


    const shareDish = (title, message, url) => {
        Share.share({
            title: title,
            message: title + ': ' + message + ' ' + url,
            url: url
        },{
            dialogTitle: 'Share ' + title
        })
    }

    if (dish != null) {
        return(
            <Animatable.View animation="fadeInDown" duration={2000} delay={1000}
            ref={this.handleViewRef}
            {...panResponder.panHandlers}>
                <Card
                featuredTitle={dish.name}
                image={{uri : baseUrl+dish.image }}>
                    <Text style={{margin: 10}}>
                        {dish.description}
                    </Text>
                    <View style={styles.formRow}>
                    <Icon
                    raised
                    reverse
                    name={ props.favorite ? 'heart' : 'heart-o'}
                    type='font-awesome'
                    color='#f50'
                    onPress={() => props.favorite ? console.log('Already favorite') : props.onPress()}
                    />
                    <Icon
                    raised
                    reverse
                    name='pencil'
                    type='font-awesome'
                    color='#6a0dad'
                    onPress={() => props.onPressComments()}
                    />

<Icon
                            raised
                            reverse
                            name='share'
                            type='font-awesome'
                            color='#51D2A8'
                            style={styles.cardItem}
                            onPress={() => shareDish(dish.name, dish.description, baseUrl + dish.image)} />

                    </View>
                </Card>
                </Animatable.View>
            );
        }
        else {
            return(<View></View>);
        }
}


function RenderComments(props){
const comments=props.comments;
const comment=props.comment;
console.log('Inside Render Comments');
console.log(comment);


const renderCommentItem = ({index,item}) => {
    

    return (
        <View key={index} style={{margin: 10}}>
            <Text style={{fontSize: 14}}>{item.comment}</Text>
            <Text style={{fontSize: 12}}>{item.rating} Stars</Text>
            <Text style={{fontSize: 12}}>{'-- ' + item.author + ', ' + item.date} </Text>
        </View>
    );
}
return(
    <Animatable.View animation="fadeInUp" duration={2000} delay={1000}>    
<Card title='Comments' >
        <FlatList 
            data={comments}
            renderItem={renderCommentItem}
            keyExtractor={item => item.id.toString()}            
            />


        </Card>
        </Animatable.View>
);
}





class Dishdetail extends Component {


    constructor(props) {
        super(props);

        this.state = {
            showModal: false,
            author:'',
        comment:'',
        rating:0

        }
    }

markFavorite(dishId) {
    this.props.postFavorite(dishId);
};

handleComment(dishId,rating,author,comment) {
    console.log(dishId,rating,author,comment);
    this.props.postComment(dishId,rating,author,comment);
};

static navigationOptions={
    title : 'Dish Details'
};

toggleModal() {
    console.log('Inside ToggleModal');
    this.setState({showModal: !this.state.showModal});
};

handleModal() {
    this.toggleModal();
};

resetForm() {
    this.setState({
        author:'', 
        comment:'',
        showModal: false
    });
}

    render(){
        const dishId=this.props.navigation.getParam('dishId','');
        
    return(
        
    <ScrollView>
    
    <RenderDish dish={this.props.dishes.dishes[+dishId]}
                    favorite={this.props.favorites.some(el => el === dishId)}
                    onPress={() => this.markFavorite(dishId)} 
                    onPressComments={() => this.handleModal()} 
                    />
    <RenderComments comments={this.props.comments.comments.filter((comment) => comment.dishId === dishId)} 
    
    />

    

    <Modal animationType = {"slide"} transparent = {false}
                    visible = {this.state.showModal}
                    onDismiss = {() => {this.toggleModal(); this.resetForm();} }
                    onRequestClose = {() => {this.toggleModal(); this.resetForm();} }>
                    <View style={{alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      marginStart :10}}>

          
                    <Rating showRating  startingValue="{1}"  
                    style={{ paddingVertical: 30 }} 
                    onFinishRating={(rating) => {this.setState({rating:rating})}}
/>
                    </View>
                    <View style={{alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      marginStart :10}}>


                        
                    <Input inputStyle= {{marginStart :10}}
                        placeholder='Author'
                        placeholderTextColor = '#a9a9a9'
                        leftIcon={{ type: 'font-awesome', name: 'user-o'  }}
                        onChangeText = {(text) => {this.setState({ author: text })}} 
                    />
                    </View>
                    
                        <View style={styles.formRow}>
                        <Input inputStyle= {{marginStart :10}}
                        placeholder='Comment'
                        placeholderTextColor = '#a9a9a9'
                        leftIcon={{ type: 'font-awesome', name: 'comment-o' }}
                        onChangeText = {(text) => {this.setState({ comment: text })}}
                    />
                        </View>

                        <View style={{
        fontSize: 10,
         fontWeight: 'bold',
         textAlign: 'center',
         color: 'white',
         marginBottom: 20
     }}>
                        <Button buttonStyle = {{ 
         backgroundColor: '#512DA8', marginTop : 30, marginStart :30,marginEnd:10,marginBottom:20}}  
                            onPress = {() =>{this.toggleModal();
                                console.log(dishId,this.state.rating,this.state.author,this.state.comment);
                            this.handleComment(dishId,this.state.rating,this.state.author,this.state.comment) ; 
                            
                            this.resetForm()}}
                            title="SUBMIT" titleStyle={{fontWeight:'bold'}}
                            />

                        <Button buttonStyle = {{ 
         backgroundColor: '#a9a9a9', marginTop : 10, marginStart :30,marginEnd:10}} 
                            onPress = {() =>{this.toggleModal(); this.resetForm();}}
                            title="CANCEL" titleStyle={{fontWeight:'bold'}}
                            />
                    </View>
                </Modal>
    </ScrollView>
    );
    }
};


const styles = StyleSheet.create({
    formRow: {
      alignItems: 'center',
      justifyContent: 'center',
      flexDirection: 'row',
      marginStart :10
      
    }
     
});

export default connect(mapStateToProps,mapDispatchToProps)(Dishdetail);

